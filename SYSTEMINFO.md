# PaloAlto Networks Panorama

Vendor: PaloAlto
Homepage: https://www.paloaltonetworks.com/

Product: Panorama
Product Page: https://www.paloaltonetworks.com/network-security/pan-os

## Introduction
We classify PaloAlto Networks Panorama into the Security/SASE domain as Panorama is a management system for the PaloAlto Firewalls. We also classify it into the Inventory domain because it contains an inventory of firewalls.

"Palo Alto Panorama provides the ability to manage all of your firewalls and security tools." 
"Has the ability to keep firewall rules consistent across your organization."
"Gain visibility to traffic and actionable events."

The PaloAlto Networks Panorama adapter can be integrated to the Itential Device Broker which will allow your PaloAlto Firewalls to be managed within the Itential Configuration Manager Application.

## Why Integrate
The PaloAlto Networks Panorama adapter from Itential is used to integrate the Itential Automation Platform (IAP) with PaloAlto Networks Panorama. With this adapter you have the ability to perform operations such as:

- Create Firewalls. 
- Configure Firewalls
- Manage Firewalls

## Additional Product Documentation
The [API documents for PaloAlto Networks Panorama 9.1](https://docs.paloaltonetworks.com/pan-os/9-1/pan-os-panorama-api)

The [API documents for PaloAlto Networks Panorama 10.1](https://docs.paloaltonetworks.com/pan-os/10-1/pan-os-panorama-api)

The [API documents for PaloAlto Networks Panorama 10.2](https://docs.paloaltonetworks.com/pan-os/10-2/pan-os-panorama-api)
