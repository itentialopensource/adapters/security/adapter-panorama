
## 0.9.1 [08-03-2023]

* updated getToken action.json to allow request token auth

See merge request itentialopensource/adapters/security/adapter-panorama!16

---

## 0.9.0 [10-17-2022]

* don't run pagination if total-count is 0

See merge request itentialopensource/adapters/security/adapter-panorama!15

---

## 0.8.4 [10-06-2022]

* Adapt 2406

See merge request itentialopensource/adapters/security/adapter-panorama!14

---

## 0.8.3 [06-25-2022]

* Changes to query params

See merge request itentialopensource/adapters/security/adapter-panorama!13

---

## 0.8.2 [06-20-2022]

* Add back in the genericXML call - also tried to make is so migrate will not break in the future

See merge request itentialopensource/adapters/security/adapter-panorama!12

---

## 0.8.1 [05-18-2022]

* Fix systemName

See merge request itentialopensource/adapters/security/adapter-panorama!11

---

## 0.8.0 [05-18-2022] & 0.7.1 [03-30-2022] & 0.7.0 [03-23-2022] & 0.6.0 [01-21-2022]

- migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/security/adapter-panorama!6

---

## 0.5.0 [11-13-2021]

- Added new calls, rename for 9&10, moves

See merge request itentialopensource/adapters/security/adapter-panorama!5

---

## 0.4.0 [11-10-2021]

- Fixing new calls and merging panorama-os adapter calls (rename)

See merge request itentialopensource/adapters/security/adapter-panorama!4

---

## 0.3.0 [11-05-2021]

- Add new calls for network as well as version 10.1 SDWAN calls that are new. Many of these calls may not work if you are integrating to older versions of panorama. None of the existing calls were changed and should work on both versions of panorama.

See merge request itentialopensource/adapters/security/adapter-panorama!3

---

## 0.2.0 [03-25-2021]

- Add new calls from postman collection

See merge request itentialopensource/adapters/security/adapter-panorama!2

---

## 0.1.3 [03-11-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-panorama!1

---

## 0.1.2 [09-17-2020]

- Bug fixes and performance improvements

See commit 135ac7e

---

## 0.1.1 [09-17-2020]

- Initial Commit

See commit 8852d00

---
